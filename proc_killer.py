import os, signal, sys

def check_kill_process(pstring):
    for line in os.popen("ps ax | grep " + pstring + " | grep -v grep | grep -v killer"):
        fields = line.split()
        pid = fields[0]
        os.kill(int(pid), signal.SIGKILL)


if __name__ == "__main__":
    arg = sys.argv
    proc_name = arg[1]
    if proc_name is None :
        proc_name = "realtime_messaging"
    print ("ps ax | grep " + str(proc_name) + " | grep -v grep | grep -v killer")
    check_kill_process(str(proc_name))