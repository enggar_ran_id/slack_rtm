# README #

The Real Time Messaging API is a WebSocket-based API that allows you to receive events from Slack in real time and send messages as users. It's sometimes referred to as simply the "RTM API".

It is the basis for all Slack clients. It's also commonly used with the bot user integration to create helper bots for your team.

If you prefer events to be pushed to you instead, we recommend using the HTTP-based Events API instead. Most event types supported by the RTM API are also available in the Events API.

### What is this repository for? ###

* Send and receive realtime slack message 

### How do I get set up? ###

* git clone https://enggarranu@bitbucket.org/enggarranu/slack_rtm.git
* python realtime_messageing.py

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Enggar Ranu Hariawan
* Syarif Hidyatullah