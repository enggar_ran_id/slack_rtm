'''
This is an example of how to use the Slack Real Time Messaging API using
Python and the python-slackclient.
The example below originates from the python-slackclient documentation.
See https://github.com/slackhq/python-slackclient#real-time-messaging


This is everything you can do with the client:

Connect to a Slack RTM websocket. This is a persistent connection from which you can read events.
SlackClient.rtm_connect()

Read all data from the RTM websocket. Multiple events may be returned,
always returns a list [], which is empty if there are no incoming messages.
SlackClient.rtm_read()

Sends the text in [message] to [channel], which can be a name or identifier i.e. "#general" or "C182391"
SlackClient.rtm_send_message([channel, message])
e.g. sc.rtm_send_message('#general', 'https://m.popkey.co/988a17/3RkD5.gif')

Call the Slack method [method]. Arguments can be passed as kwargs, for instance: sc.api_call('users.info', user='U0L85V3B4')
SlackClient.api_call([method])

Send a JSON message directly to the websocket.
See RTM documentation for allowed types https://api.slack.com/rtm
SlackClient.server.send_to_websocket([data])

The identifier can be either name or Slack channel ID.
SlackClient.server.channels.find([identifier])

Send message [text] to [int] channel in the channels list.
SlackClient.server.channels[int].send_message([text])

Send message [text] to channel [identifier], which can be either channel name or ID. Ex "#general" or "C182391"
SlackClient.server.channels.find([identifier]).send_message([text])

Server object owns the websocket and all nested channel information.
SlackClient.server

A searchable list of all known channels within the parent server. Call print (sc instance) to see the entire list.
SlackClient.server.channels

'''

import time
import json

import re
from slackclient import SlackClient
from subprocess import call
import requests
import datefinder as datefinder
import datetime

token = 'xoxb-184016873076-DjCaMY8OQj9WXpgqmJc9eg0y'# found at https://api.slack.com/web#authentication
HEADER_URL = "http://ff84e3af.ngrok.io"
CHANNEL = "reporting_log"
sc = SlackClient(token)

if sc.rtm_connect():  # connect to a Slack RTM websocket
    print sc.rtm_read()
    while True:
        a = sc.rtm_read()# read all data from the RTM websocket
        if len(a) > 0 :
            data = a[0]
            print data
            for i in data:
                if i == "text" :
                    # get difference lastbalance
                    if ("difference" in data["text"] and "lastbalance" in data["text"]) or "perbedaan" in data["text"] :
                        sc.rtm_send_message(CHANNEL,"Calling SyariBot....")
                        a = requests.get(HEADER_URL).content
                        sc.rtm_send_message(CHANNEL, a)
                        print a

                    if "jumlah transaksi" in data["text"] :
                        match = list(datefinder.find_dates(data["text"]))
                        print match
                        if len(match) > 0 and len(match) == 2:
                            datestart = match[0].date()
                            dateend = match[1].date()
                            result = re.findall('\d{12}', data["text"])
                            result = result[0] if result else ""
                            result = str(result)
                            param = "/total_transaksi/?id_account="+result+"&date_start="+str(datestart)+"&date_end="+str(dateend)
                            a = requests.get(HEADER_URL+param).content
                            sc.rtm_send_message(CHANNEL,a)
                        else :
                            sc.rtm_send_message(CHANNEL, "# Tanggal berapa borr???")




                    # # get lastbalance agent by date --
                    # if ("lastbalance" in data["text"] and "agent" in data["text"] and "#" not in data["text"]) and \
                    #         ("dari" not in data["text"] and "antara" not in data["text"] and "dan" not in data["text"] and "sampai" not in data["text"] and "between" not in data["text"] and "and" not in data["text"])\
                    #         or "lb_agent_by_date" in data["text"] or re.match(r'\d{4}-\d{2}-\d{2}',data["text"]):
                    #     # validate tanggal
                    #         match = list(datefinder.find_dates(data["text"]))
                    #         if len(match) > 0:
                    #             datestart = match[0].date()
                    #             sc.rtm_send_message(CHANNEL, "# getting lastbalance agent @ "+ str(datestart)+". please wait!")
                    #         elif "#" not in data["text"]:
                    #             sc.rtm_send_message(CHANNEL, "# Tanggal berapa borr???")
                    #
                    # if ("lastbalance" in data["text"] and "agent" in data["text"] and "#" not in data[
                    #     "text"]) and ("dari" in data["text"] or "antara" in data["text"] or "dan" in data["text"] or
                    #                                                  "sampai" in data["text"] or "between" in data["text"] or "and" in data["text"]) or "lb_agent_betwen" in data["text"] and "dari" in data["text"] and "sampai" in data["text"]:
                    #     # validate tanggal
                    #     match = list(datefinder.find_dates(data["text"]))
                    #     if len(match) > 0:
                    #         datestart = match[0].date()
                    #         dateend = match[1].date()
                    #         sc.rtm_send_message(CHANNEL,
                    #                             "# getting lastbalance agent between @ " + str(datestart) + " and "+str(dateend) +". please wait!")
                    #         # payload = {'key1': 'value1', 'key2': 'value2'}
                    #         # r = requests.post("http://httpbin.org/post", data=payload)
                    #         # print r.text
                    #     elif "#" not in data["text"]:
                    #         sc.rtm_send_message(CHANNEL, "# dari tanggal berapa sampai berapa borr???")

        time.sleep(1)
else:
    print 'Connection Failed, invalid token?'
